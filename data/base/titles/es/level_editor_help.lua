---------------------------------------------------------------------
-- This file is part of Freedroid
--
-- Freedroid is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 2 of the License, or
-- (at your option) any later version.
--
-- Freedroid is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with Freedroid; see the file COPYING. If not, write to the
-- Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
-- MA 02111-1307 USA
----------------------------------------------------------------------

title_screen{
song = "Bleostrada.ogg",
text = [[
            EL EDITOR DE NIVELES DE FREEDROIDRPG

=== INTRODUCCIÓN === 

FreedroidRPG viene con un editor de niveles integrado. Este editor de niveles te permite controlar todo aspecto de un mapa normal de FreedroidRPG y guardar mapas.

Puedes acceder al mismo desde el menú principal (click en "Editor de Niveles") o ejecutando 'freedroidRPG -e'.

    --- Herramientas ---
Para conmutar descripciones de la interfaz mientras el puntero señala, haga click al botón de burbuja de conversación localizado cerca del borde derecho de la ventana (fila inferior de botones).

    --- Más detalles ---
Los detalles adicionales sobre obstáculos y objetos aparecerán si les haces click derecho en el selector superior de objetos.

    --- Navegación ---
Para cambiar el nivel actual, haga click en el número de un nivel cercano en el minimapa de la esquina inferior derecha, o seleccione el nivel deseado desde el menú del editor.

    --- Edición de mapas ---
Hay cuatro modos de edición: edición de Obstáculos, edición de Suelo, edición de Objetos, y edición de Waypoints.

El botón seleccionado abajo a la derecha indica los objetos que puedes seleccionar o colocar.
Cuando un botón está seleccionado, y estás en un modo de colocación, el objeto que colocarás es indicado por el selector de la parte superior de la pantalla. La selección en el mismo se halla dividida por las pestañas inmediatamente bajo el mismo.

Puedes seleccionar el tipo de obstáculo que quieras que sea colocado en el mapa en el selector de objeto superior. Simplemente haz click en él para seleccionarlo. Los obstáculos se dividen en grupos para proveer mejor visión general.

Presionando espacio, entrarás en el modo de selección siendo indicado por el cursor cambiado. Sólo puedes seleccionar grupos de objetos representados por el modo de objeto activado.
Nota importante: Sólo serás capaz de seleccionar cosas que se incluyen en el modo seleccionado, si estás en modo obstáculo, no podrás seleccionar objetos o baldosas de suelo.


        Modo de edición de Obstáculo:

Para seleccionar este modo, haga click en el botón que dice 'Obstáculo' del selector de categoría en el área inferior izquierda.
Teniendo seleccionado un obstáculo, simplemente haga click en algún lugar del mapa para colocarlo en la posición del cursor.
Puesto que hacer click es un poco impreciso, también puede usar su teclado numérico para posicionar obstáculos.
Haga click al más a la izquierda de los cinco botones sobre el selector de categoría para tener una cuadrícula con números. Use el click derecho para activarla y click derecho para cambiar el modo de la misma.
Estos números se refieren a los números de su teclado numérico si dispone de uno. Presionar '1' colocará el obstáculo seleccionado en el selector en la posición del dígito '1' en la cuadrícula púrpura.
Puesto que colocar una línea de muros es bastante ineficiente, puedes simplemente mantener el click izquierdo y una línea de muros es colocada mientras mueves el puntero si tienes un objeto muro seleccionado. Esto funciona con los muros más comunes en FreedroidRPG.
Mientras sujete el botón izquierdo colocando muros, un click derecho eliminará todos los muros que dibujó tras comenzar a presionar el botón izquierdo.
Hay algunos objetos especiales. Los muros de vidrio y los muros de ladrillos agrietados, pero también barriles y cajas pueden ser destruidos con unos pocos golpes, mientras que los dos últimos podrían expulsar objetos. Los cofres pueden ser abiertos y también pueden contener objetos.
El símbolo con huellas tachadas no es realmente un objeto sino un área de bloqueo invisible ('rectángulo de colisión'). Los rectángulos de colisión son el núcleo de todo obstáculo puesto que previenen simplemente caminar a través de ellos como puede hacerse con las baldosas de suelo o los puntos de camino.

            Selección de obstáculos

Manteniendo el botón izquierdo del mouse puede seleccionar un rectángulo de obstáculos. Tras soltar el botón, los obstáculos seleccionados se tornarán de un color distinto indicando que se hallan seleccionando. Para seleccionar obstáculos fuera del rectángulo, mantenga 'Ctrl' y haga click en el obstáculo o dibuje otro rectángulo.
Usted podría haber automáticamente seleccionado varios obstáculos con un click. Puede cambiar entre ellos haciendo click en el icono con la silla y la estantería, o presionando 'n'.
El icono con el cubo de basura puede borrar el obstáculo seleccionado.
También puede cortar (Ctrl+x, purede ser usado para borrar mediante no pegarlos ;) ), copiar (Ctrl+c) y pegar (Ctrl+v) obstáculos cortados o copiados.
Puedes mover obstáculos seleccionados manteniendo shift izquierdo y arrastrando el objeto. Sin embargo, esto puede ser bastante impreciso.

            Colocar objetos en cofres

Simplemente seleccione el cofre deseado y haga click en el botón más a la izquierda en la fila superior de botones.
Se te pasará a una pantalla similar a la pantalla de tienda.
Se mostrará un cuchillo (que en realidad no se halla en el cofre por cierto), selecciónelo y haga click en 'vender'.
Seleccione los objetos que quiere que sean expulsados cuando el jugador abre el cofre.
Estos objetos se mostrarán en la barra de compra superior.
Para eliminar uno de estos, simplemente selecciónelo y haga click en 'comprar'.
La cruz roja le saca de la pantalla.

            Añadir texto a un cartel

Seleccione el cartel y añada una etiqueta de obstáculo con el texto del cartel. Guarde el mapa y salga.
Abra el documento del nivel (map/levels.dat) y halle la nueva etiqueta de obstáculo. Cambie la línea sobre el texto de 'type=30' ' a type=32' y guarde.
Ahora cuando haga click en el cartel en el juego su mensaje corto aparecerá.

            Añadir un diálogo a una terminal

Seleccione el terminal y añada una etiqueta de obstáculo con el nombre de diálogo que desee usar. Guarde el mapa y salga.
Abra el documento de nivel (map/levels.dat) y halle la nueva etiqueta de obstáculo.
Cambie la linea de arriba del texto de "type=30" a "type=32" y guarde. Ahora, al hacer click en la terminal en el juego, comenzará el dialogo seleccionado.

        Modo editor de suelo:

El modo editor de suelo funciona de forma similar al modo editor de obstáculos. Puedes seleccionar distintos tipos de suelos en el seleccionador de objetos.
Para llenar una zona con un solo tipo de suelo, primero selecciona la baldosa que deseas usar. Luego, haz click izquierdo y arrastra hasta cubrir la zona deseada. Las baldosas se ubicarán en la capa de suelo actual.
Los distintos tipos de suelo no tienen nada en especial. Son mera decoración.

La visibilidad de las capas de suelo se puede controlar mediante un botón con el ícono de capa. Dicho botón solo se muestra en niveles con varias capas.
El click izquierdo en el botón cambia la visibilidad de una o todas las capas de suelo. El click derecho cambia la capa actual.

            Selección de tipo de suelo

La selección es tan sencilla como en el modo obstáculo. Puedes mover las baldosas con el método descrito anteriormente.
En los niveles con varias capas, solo se seleccionan las capas visibles. Si solo una capa es visible, puede seleccionar solamente las baldosas de esa capa.

Para ver solamente el suelo, haga click en el icono de la lámpara para ocultar los obstáculos. Para volverlos a mostrar, haga click nuevamente.
El icono con los rectángulos turquesa muestra los rectángulos de colisión. Estos indican el área de bloqueo de un obstáculo. Tux no puede caminar en dicha área.
Si lo activas y luego pruebas (explicado más adelante) tu mapa, los rectángulos se seguirán mostrando si están activados. Esto es bastante útil para probar si se puede pasar o no por un espacio.

        Modo editor de objeto:

También puedes ubicar objetos para ser usados en el mapa por quien juega.
Estos objetos pueden tomarse, llevarse, y algunos incluso pueden usarse o equiparse.
Algunos objetos se usan para avanzar en la trama, otros otorgan bonificaciones, mientras que otros no tienen efecto.
Selecciona el modo objeto y haz click en un objeto que se muestra en el selector. Algunos requieren que se especifique una cantidad antes de que se puedan colocar.
Puedes ajustar la cantidad haciendo click en los botones de flecha, o moviendo la bola azul a la izquierda o a la derecha.
Pulsa "g" para tener una mejor vision de los objetos disponibles (también se puede usar para soltar objetos, que se colocarán en el punto de mira). Aprieta "Esc" para cancelar el proceso sin soltar nada.
También puedes hacer click en el icono con las botas cruzadas.


        Modo editor de waypoint:

Actualmente, los droides (todos los personajes no jugables) se mueven por los niveles según waypoints predefinidos.
Pulsa W para fijar un waypoint. Esto alternará el waypoint en el rectángulo bajo la mira.
Teniendo este modo activado, también puedes hacer click en la posición del mapa donde quieras tener un waypoint. Si haces otro click en otro lado, se fija un nuevo waypoint, conectado automáticamente con el anterior.
Hacer click en un waypoint ya existente te permite conectarlo con otro. Solo tienes que hacer click también en ese otro para hacerlo.
Sin embargo, hay una diferencia entre ambos métodos. Si conectas dos waypoints con un teclado, las conexiones serán unidireccionales.
Es decir, si se crea una conexión desde el waypoint A al waypoint B, el robot solo se moverá desde A a B, pero no al revés.
Para eliminar la conexión unidireccional, puedes "superponerla" con una conexión en la dirección opuesta (no funciona con conexiones bidireccionales).
Sin embargo, las conexiones bidireccionales se crean de forma automática cuando se conectan waypoints haciendo click.
Importante: ¡no es posible conectar waypoints de mapas distintos!
Los waypoints también se utilizan para colocar robots generados de forma aleatoria. Sin embargo, esto puede no ser apropiado para algunos waypoints.
Hay waypoints "normales" de color blanco, que se usan para los robots generados, y hay waypoints "especiales" de color morado, que se usan para los NPC.
Puedes seleccionar distintos tipos de waypoints en la barra de selección superior. Para convertir un waypoint normal en uno morado, o viceversa, pulse mayús+w.
Asegúrate de que las rutas entre waypoints no estén bloqueadas por obstáculos.
Para revisar esto en todo el mapa de forma automática, puedes usar el validador de nivel de mapa que se explica más adelante.


        Fijar etiquetas:

Hay dos tipos de etiquetas: de mapa y de obstáculo.
Asegúrate de que cada etiqueta tenga su propia ID.
Si dejas una cadena vacía, la etiqueta respectiva se eliminará.


            Fijar etiquetas de mapa

Las etiquetas de mapa se usan para definir las ubicaciones de inicio de los NPC (ver ReturnOfTux.droids), los eventos que ocurren cuando Tux se mueve sobre ellas (ver events.dat) o las ubicaciones utilizadas para el movimiento de NPC a través de los archivos de script lua (eventos, misiones y diálogos).
Para fijar una nueva etiqueta de mapa, pulse la tecla "m" o haga click en el botón con dicha letra. Hecho esto, se le pedirá la etiqueta del mapa. Tenga en cuenta que habrán círculos de colores en cada parte del mapa que tenga una etiqueta.
La etiqueta de mapa se fijará en la baldosa de la mitad de la pantalla de forma automática.
Puedes activar/desactivar la visualización de droides/NPC pulsando el botón con el robot 302.

            Fijas etiquetas de obstáculo

Las etiquetas de obstáculo son importantes para asociar obstáculos con eventos (por ejemplo, durante una misión). Si, por ejemplo, se pretende que un evento elimine un obstáculo especial de un muro, se le debe dar un nombre o ID a este, para que se le pueda hacer referencia en la definición del evento.
También puede añadir diálogos a los obstáculos para hablar con ellos como si fueran NPC normales.
Para fijar una etiqueta en un obstáculo, primero debes seleccionarlo (ver explicación de modo obstáculo más arriba).
Haz click en el icono del carten y la O, y se te pedirá que asignes una nueva etiqueta a este obstáculo.

Puedes alternar la visualización de etiquetas usando el pequeño icono con el circulo de etiqueta.

        Guardar mapas:

Para guardar un mapa, haz click en el icono de disquete que se encuentra en la zona superior derecha de la pantalla de edición. El icono de la puerta sirve para salir del editor.
También se puede hacer esto mediante el menú que se abre al pulsar "Esc".


Consejos generales:

	Vista general
Para cambiar el factor de zoom, pulsa "o" o haz click en el icono con la lupa.
Para ver distintos tipos de factores de zoom, prueba hacer click izquierdo y derecho.


	Menú de edición

Puedes acceder a este menú pulsando ESC.

		"Nivel":
Aquí puedes navegar fácilmente hacia otros niveles. Puedes usar las flechas direccionales con esta opción seleccionada
para ir al nivel siguiente o al anterior (en cuanto a números) o, si haces click, puedes ingresar el número del nivel que quieras y pulsar intro.

		Opciones de nivel
				Nivel:	Ver información más arriba
				Nombre:	El nombre del mapa, que se muestra en el GPS de la esquina superior derecha. Puedes deshabilitar el GPS dentro del juego en el menú de opciones.
				Tamaño:	Puedes agrandar o reducir el tamaño del nivel. Selecciona el borde donde quieres añadir/eliminar baldosas y haz click en los botones de flecha.
				Capas de suelo: Usa los botones de flecha para cambiar el número de capas de suelo del nivel actual.
				Niveles adyacentes:	Puedes establecer los niveles que rodean el nivel actual. Para esto, ingresa el número de nivel en el borde correspondiente.
								Un nivel solo puede tener un nivel adyacente (con el que comparte borde) en cada uno de los puntos cardinales (norte, sur, este, oeste).
				Mazmorra aleatoria:	Al activar esta opción, el mapa generará una mazmorra automáticamente. También puedes establecer el número de teletransportadores desde y hacia el mapa.
								Las mazmorras generadas de forma aleatoria ya tienen configurado todo lo necesario, como waypoints, robots y obstáculos.
				Clase de objeto para obstáculos:	Establece que clases de objetos deben ser arrojados por barriles/cofres/cajas.
				Bloqueo de teletransportador:	Hace que sea (im)posible salir del nivel.
				Par de teletransportadores:	Esta opción es importante si creas una mazmorra que no está conectada directamente a otro mapa. Aquí puedes establecer el número de entradas y salidas de una mazmorra aleatoria.
				Iluminación:			¿Cuánta luz quieres que haya? Pulsa espacio para alternar entre el modo de iluminación ambiental (brillo general del mapa) y el modo de iluminación extra (luz emitida por obstáculos, como lámparas o setas).
				Música de fondo:	Aquí puedes establecer la música de fondo que suena mientras se está en el mapa. Las pistas disponibles se pueden encontrar en ./sound/music/ .
									Tan solo ingresa el nombre del archivo con la extensión .ogg incluída.
				Resistencia infinita para correr:	Al activar esta opción, la estamina de Tux no se gastará al correr por el mapa. esta opción debe usarse solo en niveles que no tienen NPC hostiles, como por ejemplo el nivel 0, la ciudad.
				Añadir/eliminar nivel:		Te permite añadir un nivel nuevo o eliminar el nivel actual.

		Opciones avanzadas
Aquí puedes ejecutar el validador de nivel de mapa.
El validador de nivel de mapa verifica que las rutas entre waypoints no estén bloqueadas por obstáculos. Puedes encontrar una salida más detallada, que enumera las conexiones bloqueadas, en la terminal (si se ejecuta en segundo plano) o un archivo de salida de error global.
Además, puedes comprobar si hay obstáculos en el borde del mapa en puntos críticos.
Esta opción SIEMPRE debe ser ejecutada antes de que el mapa esté listo.
También puedes ejecutar "freedroidRPG -b leveltest" para verificar.

		Mapas de prueba
Te permite probar tus modificaciones con facilidad.
Si sales de este modo, los cambios de obstáculos hechos, como destruir cajas, volverán al estado en que estaban cuando comenzaste la prueba.




Teclas:
espacio					alternar fijar/seleccionar
w						fijar waypoint
shift+w					alterna waypoints de "robot aleatorio" o "NPC"
esc					menú de acceso
teclado numérico 1-9	fijar obstáculos en las posiciones respectivas de la cuadrícula
n						repetir obstáculos seleccionados (siguiente)
z						deshacer última acción
y						rehacer última acción deshecha
c						establecer rutas entre waypoints
ctrl+x o retroceso		corta el objeto seleccionado y también puede usarse para eliminar.
ctrl+c					copiar selección
ctrl+v					pegar objeto(s) cortados/copiados
alt+shift				arrastrar/mover selección usando el ratón
flechas direccionales				desplazarse por el mapa
ctrl+flechas direccionales			desplazarse a mayor escala
rueda del ratón				navega por los obstáculos del selector de objetos
ctrl+re pág/av pág	navega por los obstáculos del selector de objetos
g						accede a la pantalla de selección de objetos para colocar
t						alternar mostrar/ocultar transparencia 3x3 alrededor de la mira
m						añadir/editar una etiqueta de mapa en la baldosa seleccionada
o						zoom
tab						cambiar al siguiente modo de edición
mayús+tab				cambiar al modo de edición anterior
f						cambiar a la siguiente pestaña de objetos
mayús+f					cambiar a la pestaña de objetos anterior


Contáctanos si encuentras problemas con el editor.
Además, no tengas miedo de enviarnos mapas si es que creas algo genial. No mordemos. :)
]]
}
